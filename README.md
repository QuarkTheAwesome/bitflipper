# bitflipper
A program to generate somewhat interesting glitch art.

## How does it work?
Given an image file, usually a .jpg, bitflipper will flip each bit in the range you gave and see if the resulting image is perceptually different (using [dssim](https://github.com/kornelski/dssim)). If it is, the image is saved. This can produce cool images in the right circumstances. Any given output file only has a single bitflip.

## Usage example
```shell
mkdir out
# -s and -e are bit numbers
bitflipper -s 100 -e 1000 input.jpg out
```

## Building
```shell
cargo build --release
```
