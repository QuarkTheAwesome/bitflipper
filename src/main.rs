use std::fs;
use std::env;
use std::path::Path;
use std::ffi::OsStr;

use dssim_core::*;
use imgref::Img;
use load_image::ImageData;
use getopts::Options;

fn load_dssim(attr: &Dssim, img: &load_image::Image) -> Option<DssimImage<f32>> {
    match img.bitmap {
        ImageData::RGB8(ref bitmap) => attr.create_image(&Img::new(bitmap.to_rgblu(), img.width, img.height)),
        ImageData::RGB16(ref bitmap) => attr.create_image(&Img::new(bitmap.to_rgblu(), img.width, img.height)),
        ImageData::RGBA8(ref bitmap) => attr.create_image(&Img::new(bitmap.to_rgbaplu(), img.width, img.height)),
        ImageData::RGBA16(ref bitmap) => attr.create_image(&Img::new(bitmap.to_rgbaplu(), img.width, img.height)),
        ImageData::GRAY8(ref bitmap) => attr.create_image(&Img::new(bitmap.to_rgblu(), img.width, img.height)),
        ImageData::GRAY16(ref bitmap) => attr.create_image(&Img::new(bitmap.to_rgblu(), img.width, img.height)),
        ImageData::GRAYA8(ref bitmap) => attr.create_image(&Img::new(bitmap.to_rgbaplu(), img.width, img.height)),
        ImageData::GRAYA16(ref bitmap) => attr.create_image(&Img::new(bitmap.to_rgbaplu(), img.width, img.height)),
    }
}

fn mutate(source: &Vec<u8>, flip: u64) -> Result<Vec<u8>, std::num::TryFromIntError> {
    let index: usize = (flip >> 3).try_into()?;
    let bit: u8 = (1 << (flip & 7)).try_into()?;

    let mut result = source.clone();
    result[index] = result[index] ^ bit;
    Ok(result)
}

fn run(infile: &Path, outdir: &Path, start: u64, end: u64, log_findings: bool, thresh: f64) -> Result<(), std::io::Error> {
    let source = fs::read(infile)?;
    let source_img = load_image::load_data(&source).unwrap();

    let ssim = Dssim::new();
    let source_ssim = load_dssim(&ssim, &source_img).unwrap();

    for id in start..end {
        if id % 100 == 0 {
            let percent = ((id - start) * 100) / (end - start);
            println!("Working: {}% - {}/{}", percent, id, end);
        }
        let candidate = match mutate(&source, id) {
            Ok(img) => img,
            Err(error) => {
                println!("Failed to apply mutation {}: {:?}", id, error);
                break;
            },
        };

        let candidate_img = match load_image::load_data(&candidate) {
            Ok(img) => img,
            Err(_) => continue,
        };

        //causes panics in dssim
        if (candidate_img.width != source_img.width) ||
           (candidate_img.height != source_img.height) { continue };

        let candidate_ssim = load_dssim(&ssim, &candidate_img).unwrap();
        let (difference, _) = ssim.compare(&source_ssim, &candidate_ssim);
        if difference < thresh { continue };

        let mut path = outdir.to_path_buf();
        path.push(id.to_string());
        path.set_extension(infile.extension().unwrap_or(OsStr::new("jpg")));

        if log_findings {
            println!("Found image {}, score {} - saving to {}", id, difference, path.display());
        }
        fs::write(path, candidate)?;
    }

    Ok(())
}

fn usage(program: &str, opts: Options) {
    let brief = format!("Usage: {} [options] FILE OUTDIR", program);
    print!("{}", opts.usage(&brief));
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();

    let mut opts = Options::new();
    opts.optopt("s", "start", "Start of bitflip range", "NUM");
    opts.optopt("e", "end", "End of bitflip range", "NUM");
    opts.optopt("t", "thresh", "Interest threshold, decimal", "N.NNN");

    opts.optflag("q", "quiet", "Don't log images as they're found");
    opts.optflag("h", "help", "Prints help menu");

    let matches = opts.parse(&args[1..]).unwrap();
    if matches.opt_present("h") {
        usage(&program, opts);
        return;
    }
    let log_findings = !matches.opt_present("q");

    if matches.free.len() != 2 {
        usage(&program, opts);
        return;
    }
    let infile = Path::new(&matches.free[0]);
    let outdir = Path::new(&matches.free[1]);

    if !outdir.is_dir() {
        println!("Output folder \"{}\" invalid (does it exist?)", outdir.display());
        usage(&program, opts);
        return;
    }

    if log_findings {
        println!("Writing {} mutations to {}", infile.display(), outdir.display());
    }

    let start = match matches.opt_get_default::<u64>("s", 200) {
        Ok(val) => val,
        Err(err) => {
            println!("Error parsing start value: {:?}", err);
            usage(&program, opts);
            return;
        }
    };
    let end = match matches.opt_get_default::<u64>("e", 5000) {
        Ok(val) => val,
        Err(err) => {
            println!("Error parsing end value: {:?}", err);
            usage(&program, opts);
            return;
        },
    };
    let thresh = match matches.opt_get_default::<f64>("t", 0.01) {
        Ok(val) => val,
        Err(err) => {
            println!("Error parsing end value: {:?}", err);
            usage(&program, opts);
            return;
        },
    };

    match run(&infile, &outdir, start, end, log_findings, thresh) {
        Err(err) => { println!("{:?}", err) },
        _ => {},
    };
}
